def call() {
    pipeline {

        agent any

        tools {
            maven 'Maven 3.5.2'
            jdk 'jdk8'
        }

        environment {
            ARTIFACT_ID = readMavenPom().getArtifactId()
        }

        stages {
            stage ('Check versions') {
                when {
                    branch 'master'
                }
                steps {
                    bat "mvn enforcer:enforce@enforce-no-snapshots"
                }
            }
            stage ('Build') {
                steps {
                        /*
                         * El mvn package de cloud no hace -U para no volver a descargarse la metadata
                         * de las dependencias y tampoco hace clean para aprovechar el building anterior
                         */
                        bat '''
                            call mvn --batch-mode -V -U -e clean package -DskipTests
                            if "%ARTIFACT_ID%"=="webSupervisor" (
                                call mvn --batch-mode -V -e package -DskipTests -Pcloud
                            ) 
                        '''
                }
            }
            stage ('Tests') {
                steps {
                    bat "mvn --batch-mode -V -e test -Dsurefire.useFile=false"
                }
            }
            stage ('Deploy to Artifactory') {
                when {
                    expression {
                        /*
                         * Desplegamos en Artifactory si el proyecto no es webSupervisor o si lo es y
                         * estamos en la rama master. Esto funciona porque el multibranch está configurado
                         * sólo para las ramas master y develop. 
                         */
                        anyOf {
                            expression {
                                ARTIFACT_ID != 'webSupervisor'
                            }
                            branch 'master'
                        }
                    }
                }
                steps {
                    configFileProvider([configFile(fileId: 'e364f256-5ad5-4337-ace0-8b218fa26c2c', variable: 'MAVEN_SETTINGS')]) {
                        bat "mvn --batch-mode -V -e -s %MAVEN_SETTINGS% deploy"
                    }
                }
            }
        }

        post {
            always {
                deleteDir()
            }
            failure {
                emailext (
                    recipientProviders: [[$class: 'DevelopersRecipientProvider']]
                )
            }
        }

    }
}
